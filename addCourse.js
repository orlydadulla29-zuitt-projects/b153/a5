/*
Activity: Finish the add course functionality for our app. Use the following as your instructions:

1. Get the #createCourse element for the HTML file via querySelector
2. Make sure that the form's default behavior is prevented.
3. Get the input values for #courseName, #courseDescription, and #coursePrice
4. Use a POST request to send the input's values to the /courses endpoint
5. Make sure that you include the proper authorization header and its value
6. If successful, redirect the user to /courses.html. If not, show an error alert.
7. Make sure courses are successfully added.
*/

let createCourseForm = document.querySelector("#createCourse")

createCourseForm.addEventListener("submit", (event) => {
	event.preventDefault() 

	let courseName = document.querySelector("#courseName").value
	let courseDescription = document.querySelector("#courseDescription").value
	let coursePrice = document.querySelector("#coursePrice").value

	fetch("http://localhost:4000/courses", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${localStorage.getItem("token")}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		if(data === true){
			window.location.replace("courses.html")
		}else{
			alert("Course not added. Please try again")
		}
	})
})